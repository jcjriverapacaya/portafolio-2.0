/* ---------- Variables ---------*/

let toggler = document.getElementById('toggler')

/* ---------- Function ---------*/

const ActiveNav  = () => {
  let nav = document.getElementById('nav'),
      container = document.getElementById('container')

  nav.classList.toggle('is-active')
  toggler.classList.toggle('is-active')
  container.classList.toggle('is-active')


}


/* ---------- Listeners ---------*/

toggler.addEventListener('click',ActiveNav)